from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Signature import pkcs1_15

from util import create_hash


def keygen():
    key = RSA.generate(2048)
    private_key = key.export_key()
    file_out = open("./encryptKeys/private_1.pem", "wb")
    file_out.write(private_key)
    file_out.close()

    public_key = key.publickey().export_key()
    file_out = open("./encryptKeys/receiver_1.pem", "wb")
    file_out.write(public_key)
    file_out.close()
    print(public_key)


def sign(data):
    private_key = RSA.import_key(open("./encryptKeys/private_1.pem").read())
    signer = pkcs1_15.new(private_key)
    return signer.sign(data)


def verify_sig(signature, public_key, data_hash):
    signer = pkcs1_15.new(public_key)
    is_valid = False
    try:
        signer.verify(data_hash, signature)
        is_valid = True
    except (ValueError, TypeError):
        is_valid = False

    return is_valid


def encrypt(data):
    file_out = open("encrypted_data.bin", "wb")
    public_key = RSA.import_key(open("./encryptKeys/receiver_1.pem").read())
    ###################
    session_key = get_random_bytes(16)

    cipher_rsa = PKCS1_OAEP.new(public_key)
    enc_session_key = cipher_rsa.encrypt(session_key)
    cipher_aes = AES.new(session_key, AES.MODE_EAX)
    ciphertext, tag = cipher_aes.encrypt_and_digest(data.encode("utf-8"))
    [file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext)]
    file_out.close()


def decrypt(enc_data):

    file_in = open("encrypted_data.bin", "rb")

    private_key = RSA.import_key(open("./encryptKeys/private_1.pem").read())

    enc_session_key, nonce, tag, ciphertext = [
        file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)
    ]

    # Decrypt the session key with the private RSA key
    cipher_rsa = PKCS1_OAEP.new(private_key)
    session_key = cipher_rsa.decrypt(enc_session_key)

    # Decrypt the data with the AES session key
    cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
    data = cipher_aes.decrypt_and_verify(ciphertext, tag)
    print(data.decode("utf-8"))
