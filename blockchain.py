from block import Block
from transaction import Transaction


class Blockchain:
    def __init__(self):
        self.block_chain = []
        self.pending_transactions = []

    def add_transaction(self, transact):
        index = 0
        if len(self.pending_transactions) > 0:
            index = self.pending_transactions[-1].index + 1

        # type check to see if we can make a transaction object form a string or if it is the right thing already
        if isinstance(transact, Transaction):
            transact.index = index
            self.pending_transactions.append(transact)
        elif transact is not None:
            self.pending_transactions.append(
                Transaction(
                    transact["sender"],
                    transact["reciever"],
                    transact["amount"],
                    index,
                    transact["other"],
                )
            )
        else:
            raise TypeError

    def can_make_block(self):
        transacts = ""
        for tr in self.pending_transactions:
            transacts += str(tr)
            if len(bytes(f"{transacts}", "UTF-8")) // 1023 >= 1:
                return True
        return False

    def mine_block(self):
        transacts = ""
        actual_transactions = []
        i = 0
        is_full = False
        # find where the last transaction for this block is - don't remove those transactions from the list until block is mined
        for tr in self.pending_transactions:
            transacts += str(tr)
            actual_transactions.append(tr)
            i += 1
            # are string of transactions around 1MBish
            if len(bytes(f"{transacts}", "UTF-8")) // 1023 >= 1:
                is_full = True
                break

        if is_full:
            # make the block and add the transactions
            block = Block(self.block_chain[-1].my_hash, self.block_chain[-1].index)
            for t in actual_transactions:
                block.add_transaction(t)
            # mine the block and perform proof of work
            block.sign_and_seal()

            print(f'Adding {len(actual_transactions)} transactions to block {block.index}')
            # add block and remove those transactions from pending
            self.block_chain.append(block)
            self.pending_transactions = self.pending_transactions[i:]

    def __str__(self):
        rv=''
        for b in self.block_chain:
            rv+=str(b)
        return rv
