from block import Block, create_hash_digest
from blockchain import Blockchain
import random
from Crypto.PublicKey import RSA

# source .venv/bin/activate
# (.venv) will@ubunreciever:~/Documents/crypreciever$ nodemon --exec python main.py

def block_ex():

    blockchain = Blockchain()
    blocks = []

    block1 = Block(0)
    block1.sign_and_seal()
    blockchain.block_chain.append(block1)

    for x in range(0, 20):
        blockchain.add_transaction(
            {
                "other": "some other data in ehere idk",
                "amount": -100 + 200*random.random(),
                "reciever": "hopper",
                "sender": "funreciever",
            }
        )
    if blockchain.can_make_block():
        blockchain.mine_block()
    print(blockchain)


def main():
#    keygen()
    blockchain = Blockchain()
    blocks = []

    block1 = Block(0)
    block1.sign_and_seal()
    blockchain.block_chain.append(block1)

    blockchain.add_transaction(
        {
            "other": "some other data in ehere idk",
            "amount": -100 + 200*random.random(),
            "reciever": "hopper",
            "sender": "funreciever",
        }
    )

    public_key = RSA.import_key(open("./encryptKeys/receiver_1.pem").read())
    print( blockchain.pending_transactions[0].is_verified_transaction(public_key))



if __name__ == "__main__":
    main()
