from Crypto.Hash import SHA256


def create_hash(newVal):
    return SHA256.new(bytes(newVal, "UTF-8"))

def create_hash_digest(newVal):
    return SHA256.new(bytes(newVal, "UTF-8")).hexdigest()
