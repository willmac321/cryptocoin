import datetime
import math

from transaction import Transaction
from util import create_hash_digest


class Block:
    def __init__(self, parent_hash, parent_index=None):
        self.parent_hash = parent_hash
        self.transactions = []
        self.created_at = datetime.datetime.utcnow()
        self.index = parent_index + 1 if parent_index is not None else 0
        self.nonce = 0
        self.my_hash = None

    def __str__(self):
        return f'{"{"}\n\tindex: {self.index}\n\tparent_hash: {self.parent_hash}\n\ttimestamp: {self.created_at}\n\tnonce: {self.nonce}\n\tmy_hash: {self.my_hash}\n\ttransactions:\n\t\t{(chr(10)+chr(9)+chr(9)).join(map(str,self.transactions))}\n{"}"}'

    def to_string(self):
        return f'{self.index}::{self.parent_hash}:{self.created_at}:{"".join(map(str,self.transactions))}:::{self.nonce}'

    def add_transaction(self, transact):
        index = 0
        if len(self.transactions) > 0:
            index = self.transactions[-1].index + 1

        if isinstance(transact, Transaction):
            transact.index = index
            transact.create_signature()
            self.transactions.append(transact)
        elif transact is not None:
            tran = Transaction(
                transact["sender"],
                transact["reciever"],
                transact["amount"],
                index,
                transact["other"],
            )
            tran.create_signature()
            self.transactions.append(tran)
        else:
            raise TypeError

    def get_hash(self):
        return self.my_hash

    def get_size(self):
        return len(bytes(f"{self.to_string()}", "UTF-8"))

    def sign_and_seal(self):
        self.proof_of_work("bed")

    def proof_of_work(self, criteria):
        self.my_hash = create_hash_digest(self.to_string())
        self.nonce = 0
        while self.my_hash[: len(criteria)] != criteria:
            self.nonce += 1
            self.my_hash = create_hash_digest(self.to_string())
