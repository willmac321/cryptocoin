import datetime

from util import create_hash
from data_encrypt import sign, verify_sig


class Transaction:
    def __init__(self, sender, receiver, amount, i, other=None):
        self.sender = sender
        self.receiver = receiver
        self.timestamp = datetime.datetime.utcnow()
        self.index = i
        self.amount = amount
        self.other = other
        self.hash = create_hash(str(self))
        self.signature = None

    def __str__(self):
        return f"{self.index}:{self.sender}::{self.receiver}:::{self.amount}:::{self.timestamp}:::{self.other}"

    def create_signature(self):
        self.signature = sign(self.hash)

    def is_verified_transaction(self, pub_key):
        return verify_sig(self.signature, pub_key, self.hash)


